﻿using Question2.Model;
using Question2.Model.Enums;
using System;
using System.Collections.Generic;

namespace Question2
{
    public class HighCard
    {
        private List<Card> deck;

        #region constructor

        public HighCard(Deck deckType, bool wildCard)
        {
            this.deck = getDeck(deckType, wildCard);
        }

        #endregion

        #region public methods

        public int Play(int decks, GameType gameType)
        {
            List<Card> playDeck;

            if (decks > 1)
            {
                playDeck = new List<Card>();
                foreach(Card card in deck)
                {
                    for(int i = 0 ; i < decks ; i++)
                    {
                        Card newCard = new Card(card.value, card.suit);
                        playDeck.Add(newCard);
                    }
                }
            } else
            {
                playDeck = deck;
            }

            int wildCardResult = getCards(playDeck, out Card myCard, out Card oppCard);

            if (wildCardResult != 0)
                return wildCardResult;

            // default is GameType.NO_TIES

            switch (gameType)
            {
                case GameType.WITH_TIES:
                    return myCard.value - oppCard.value;
                    break;
                case GameType.RESOLVE_BY_SUIT:
                    return resolveBySuit(myCard, oppCard);
                    break;
                default:
                    return resolveWithoutTies(playDeck, myCard, oppCard);
            }
        }

        public List<Card> getDeck(Deck deckType, bool wildCard)
        {
            List<Card> tempDeck;

            // default is Deck.NORMAL_DECK

            switch (deckType)
            {
                case Deck.BIG_DECK:
                    tempDeck = createDeck(20, wildCard);
                    break;
                default:
                    tempDeck = createDeck(13, wildCard);
                    break;
            }

            return tempDeck;
        }

        public void changeDeck(Deck deckType, bool wildCard)
        {
            this.deck = getDeck(deckType, wildCard);
        }

        #endregion

        #region private methods

        private List<Card> createDeck(int suitSize, bool wildCard)
        {
            List<Card> tempDeck = new List<Card>();

            for (int i = 1;  i <= suitSize; i++)
            {
                tempDeck.Add(new Card(i, Suit.CLUBS));
                tempDeck.Add(new Card(i, Suit.DIAMONDS));
                tempDeck.Add(new Card(i, Suit.HEARTS));
                tempDeck.Add(new Card(i, Suit.SPADES));
            }

            if(wildCard)
            {
                Random rnd = new Random();
                tempDeck[rnd.Next(tempDeck.Count)].isWildcard = true;
            }

            return tempDeck;
        }

        private int resolveBySuit(Card myCard, Card oppCard)
        {
            int result = myCard.value - oppCard.value;
            if (result != 0)
            {
                return result;
            }
            else
            {
                if(myCard.suit < oppCard.suit)
                {
                    return 1;
                } else if(myCard.suit > oppCard.suit)
                {
                    return -1;
                } else
                {
                    return 0;
                }
            }
        }

        private int getCards(List<Card> playDeck, out Card myCard, out Card oppCard)
        {
            Random rnd = new Random();

            myCard = playDeck[rnd.Next(deck.Count)];

            do
            {
                oppCard = playDeck[rnd.Next(deck.Count)];
            } while (oppCard.Equals(myCard));

            Console.WriteLine("{0} -- {1}", myCard.ToString(), oppCard.ToString());

            return checkWildcard(myCard, oppCard);
        }

        private int resolveWithoutTies(List<Card> playDeck, Card myCard, Card oppCard)
        {
            int result = myCard.value - oppCard.value;
            if (result == 0)
            {
                int wildCardResult = getCards(playDeck, out Card myNewCard, out Card oppNewCard);
                return wildCardResult == 0 ? resolveWithoutTies(playDeck, myNewCard, oppNewCard) : wildCardResult;
            } else
            {
                return result;
            }
        }

        private int checkWildcard(Card myCard, Card oppCard)
        {
            if (myCard.isWildcard)
                return 1;
            else if (oppCard.isWildcard)
                return -1;
            else
                return 0;
        }

        #endregion

    }
}