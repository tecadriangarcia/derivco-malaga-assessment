﻿using Question2.Model.Enums;
using System;

namespace Question2.Model
{
    public class Card
    {
        private int pValue;

        private Suit pSuit;

        private bool pWildCard = false;

        public Card(int value, Suit suit)
        {
            this.value = value;
            this.suit = suit;
        }

        public int value
        {
            get
            {
                return pValue;
            }
            set
            {
                pValue = value;
            }
        }

        public Suit suit
        {
            get
            {
                return pSuit;
            }
            set
            {
                pSuit = value;
            }
        }

        public bool isWildcard
        {
            get
            {
                return pWildCard;
            }
            set
            {
                pWildCard = value;
            }
        }

        public override string ToString()
        {
            return value + "|" + suit.ToString() + (isWildcard ? "(WildCard)" : "");
        }

    }
}
