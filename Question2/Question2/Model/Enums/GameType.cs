﻿namespace Question2.Model.Enums
{
    public enum GameType
    {
        WITH_TIES,
        RESOLVE_BY_SUIT,
        NO_TIES
    }
}
