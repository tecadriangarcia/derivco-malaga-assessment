﻿namespace Question2.Model.Enums
{
    public enum Suit
    {
        SPADES = 1,
        HEARTS = 2,
        DIAMONDS = 3,
        CLUBS = 4
    }
}
