﻿using System;
using System.Text;
using Question2.Model.Enums;

namespace Question2
{
    class Program
    {
        static void Main(string[] args)
        {
            HighCard game = new HighCard(Deck.NORMAL_DECK, true);

            // Code below changes the deck to a 20 card per suit one with one wildcard.
            // game.changeDeck(Deck.BIG_DECK, true);

            int numberOfDecks = 1;
            for (int i = 0 ; i < 100 ; i++)
            {
                int result1 = game.Play(numberOfDecks, GameType.RESOLVE_BY_SUIT);

                if (result1 > 0)
                    Console.WriteLine("win");
                else if (result1 < 0)
                    Console.WriteLine("lose");
                else
                    Console.WriteLine("draw");
            }
        }
    }
}