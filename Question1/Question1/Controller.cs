﻿using System;
using System.Text;
using System.Collections.Generic;

namespace Question1
{
    public class Controller
    {
        private List<char> transcode;

        #region constructors

        public Controller()
        {
            createTranscode();
        }

        #endregion

        #region public methods

        public void test()
        {
            string test_string = "This is a test string";
            
            string encodedString = encode(test_string);
            string decodedString = decode(encodedString);

            /**
             * To solve this I would use an already existing library. The commented code below does the trick too.
             */

            //encodedString = Convert.ToBase64String(Encoding.Default.GetBytes(test_string));
            //decodedString = Encoding.Default.GetString(Convert.FromBase64String(encodedString));

            if (test_string.Equals(decodedString))
                Console.WriteLine("Test succeeded");
            else
                Console.WriteLine("Test failed");
        }

        #endregion

        #region private methods

        private void createTranscode()
        {
            List<char> tempTranscode = new List<char>();

            string UpperAlpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            tempTranscode.AddRange(UpperAlpha.ToCharArray());
            tempTranscode.AddRange(UpperAlpha.ToLower().ToCharArray());
            tempTranscode.AddRange("0123456789".ToCharArray());
            tempTranscode.Add('+');
            tempTranscode.Add('/');

            this.transcode = tempTranscode;
        }

        private string encode(string input)
        {
            int lenght = input.Length;
            int remainder = lenght % 3;

            List<char> output = new List<char>();

            int reflex = 0;
            const int s = 0x3f;

            for (int j = 0; j < lenght; j++)
            {
                reflex <<= 8;
                reflex &= 0x00ffff00;
                reflex += input[j];

                int x = ((j % 3) + 1) * 2;
                int mask = s << x;
                while (mask >= s)
                {
                    int pivot = (reflex & mask) >> x;
                    output.Add(transcode[pivot]);
                    int invert = ~mask;
                    reflex &= invert;
                    mask >>= 6;
                    x -= 6;
                }
            }

            if (remainder > 0)
            {
                reflex <<= remainder == 1 ? 4 : 2;
                output.Add(transcode[reflex]);
            }

            output.Add('=');

            string outputString = new string(output.ToArray());
            Console.WriteLine("{0} --> {1}\n", input, outputString);
            return outputString;
        }

        private string decode(string input)
        {
            List<char> output = new List<char>();

            int bits = 0;
            int reflex = 0;

            foreach (char c in input)
            {
                if (c.Equals('='))
                    break;

                reflex <<= 6;
                bits += 6;
                reflex += transcode.IndexOf(c);

                while (bits >= 8)
                {
                    int mask = 0x000000ff << (bits % 8);
                    output.Add((char)((reflex & mask) >> (bits % 8)));
                    int invert = ~mask;
                    reflex &= invert;
                    bits -= 8;
                }
            }

            string outputString = new string(output.ToArray());
            Console.WriteLine("{0} --> {1}\n", input, outputString);
            return outputString;
        }

        #endregion
    }
}