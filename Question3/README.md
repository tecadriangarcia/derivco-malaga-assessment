# QUESTION 3

To support ties, instead fo using a boolean I chose to use an integer.
* If the function returns a positive value it's a win.
* If it return a negative integer it's a loss.
* If the value is 0, then its a tie.

When we create a game:
* The first parameter its an enum. It could be an integer indicating the amount of cards for each suit, but i deciced to make it an enum to support only 13 and 20 cards.
* The second one its a boolean. If true one card of the deck will be a wildcard.

Whenever we call the Play() function, first we need the amount of decks. Then we need to specify the game type:
* **WITH_TIES**: In this game mode each player draws one single card from the deck. We find if its a win/loss/tie based only on the card value.
* **RESOLVE_BY_SUIT**: Same as the last one, but in this case if the values are the same we check the suit. Suit precedence from higher to lower (SPADES, HEARTS, DIAMONDS, CLUBS).
* **NO_TIES**: In the last game mode we only check for the card value. If it's a tie, each player draws a new card until there's a winner.

It there is a wildcard in the deck and a player draws it then it's a win for that player, no matter the game type selected.

Additional information:
* First we draw a card and then the opponent draws his. If the opponet card is exactly the same as ours, the opponet draws another (To simulate that once we get a card, that exact card is no longer available).
* Whenever both players draw a card. We print both cards following the next pattern (Only if the card is the wildcard the tag is shown):

    **myCard{value | suit (isWildcard)}  ---  oppCard{value | suit (isWildcard)}**
* The deck can be changed after the game is initialized by calling changeDeck(DeckType, wildCard).
